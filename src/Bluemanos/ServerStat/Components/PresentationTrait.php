<?php

namespace Bluemanos\ServerStat\Components;

/**
 * Class PresentationTrait.
 */
trait PresentationTrait
{
    /**
     * Get human readable file size.
     *
     * @param float $bytes
     * @param int $precision
     * @return string
     */
    public function humanFileSize($bytes, $precision = 1)
    {
        $symbols = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        $class = min((int) log($bytes, 1024), count($symbols) - 1);

        return sprintf('%.'.$precision.'f ', $bytes / pow(1024, $class)).$symbols[$class];
    }

    /**
     * Colorful the progress bar.
     *
     * @param int $value
     * @return string
     */
    public function colorProgress($value)
    {
        $progressClass = 'progress-bar-success';

        if ($value > 50 && $value < 85) {
            $progressClass = 'progress-bar-warning';
        } elseif ($value >= 85) {
            $progressClass = 'progress-bar-danger';
        }

        return $progressClass;
    }
}
