<?php

namespace Bluemanos\ServerStat\Components;

/**
 * Class Hdd.
 */
class Hdd
{
    use PresentationTrait;

    /**
     * Get disk free space.
     *
     * @param string $path
     * @param bool $humanReadable
     * @return string
     */
    public function freeSpace($path = '/', $humanReadable = true)
    {
        $freeDisc = disk_free_space($path);

        return $humanReadable ? $this->humanFileSize($freeDisc) : $freeDisc;
    }

    /**
     * Get disk total space.
     *
     * @param string $path
     * @param bool $humanReadable
     * @return string
     */
    public function totalSpace($path = '/', $humanReadable = true)
    {
        $freeDisc = disk_total_space($path);

        return $humanReadable ? $this->humanFileSize($freeDisc) : $freeDisc;
    }

    /**
     * Get hdd space in percent - usage vs. total.
     *
     * @return mixed
     */
    public function hddPercent()
    {
        $freeSpace = $this->freeSpace('/', false);
        $totalSpace = $this->totalSpace('/', false);

        return (($totalSpace - $freeSpace) / $totalSpace) * 100;
    }
}
