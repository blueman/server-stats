<?php

namespace Bluemanos\ServerStat\Components;

/**
 * Class Ram.
 */
class Ram
{
    use PresentationTrait;

    /**
     * An array where memory stats are saved.
     *
     * @var array
     */
    private $serverMemory = [];

    /**
     * Get php memory usage.
     *
     * @param bool $humanReadable
     * @return string
     */
    public function getMemoryPhpUsage($humanReadable = true)
    {
        $memoryUsage = memory_get_usage();

        return $humanReadable ? $this->humanFileSize($memoryUsage) : $memoryUsage;
    }

    /**
     * Get server memory data.
     *
     * @param bool $humanReadable
     * @return array
     */
    public function getMemoryServer($humanReadable = true)
    {
        $this->serverMemory();

        return $humanReadable ? $this->serverMemory['humanReadable'] : $this->serverMemory['bytes'];
    }

    /**
     * Get server memory in percent.
     *
     * @return mixed
     */
    public function memoryServerPercent()
    {
        $this->getMemoryServer();

        return ($this->serverMemory['bytes']['used'] / $this->serverMemory['bytes']['total']) * 100;
    }

    /**
     * Get server memory data.
     *
     * @return mixed
     */
    private function serverMemory()
    {
        if (empty($this->serverMemory)) {
            $free = shell_exec('free -b');
            $free = trim($free);
            $free_arr = explode("\n", $free);
            $mem = explode(' ', $free_arr[1]);
            $mem = array_merge(array_filter($mem));

            if (count($mem) == 6) {
                $mem[6] = $mem[5];
                $mem[5] = 0;
            }

            $this->serverMemory['humanReadable'] = [
                'total' => $this->humanFileSize($mem[1]),
                'used' => $this->humanFileSize($mem[2]),
                'free' => $this->humanFileSize($mem[3]),
                'cached' => $this->humanFileSize($mem[6]),
            ];
            $this->serverMemory['bytes'] = [
                'total' => $mem[1],
                'used' => $mem[2],
                'free' => $mem[3],
                'cached' => $mem[6],
            ];
        }
    }
}
