<?php

namespace Bluemanos\ServerStat\Components;

/**
 * Class Cpu.
 */
class Cpu
{
    /**
     * Get server load.
     *
     * @param bool $compact
     * @return mixed
     */
    public function load($compact = true)
    {
        $load = sys_getloadavg();
        $compacted = implode(' ', array_map(function ($var) { return round($var, 2); }, $load));

        return $compact ? $compacted : $load;
    }

    /**
     * Get server load in percent.
     *
     * @return mixed
     */
    public function loadPercent()
    {
        return ($this->load(false)[0] / $this->cpusNumber()) * 100;
    }

    /**
     * Returns the number of available CPU cores
     * Should work for Linux, Windows, Mac & BSD.
     *
     * @return int
     */
    public function cpusNumber()
    {
        if (is_file('/proc/cpuinfo')) {
            $numCpus = $this->getCpuNumberFromLinux();
        } elseif ('WIN' == strtoupper(substr(PHP_OS, 0, 3))) {
            $numCpus = $this->getCpuNumberFromWindows();
        } else {
            $numCpus = $this->getCpuNumberFromMac();
        }

        return $numCpus;
    }

    /**
     * Get number of cpus for linux base system.
     *
     * @return int
     */
    private function getCpuNumberFromLinux()
    {
        $cpuinfo = file_get_contents('/proc/cpuinfo');
        preg_match_all('/^processor/m', $cpuinfo, $matches);
        $numCpus = count($matches[0]);

        return intval($numCpus);
    }

    /**
     * Get number of cpus for windows base system.
     *
     * @return mixed
     */
    private function getCpuNumberFromWindows()
    {
        $numCpus = 1;

        $process = @popen('wmic cpu get NumberOfCores', 'rb');

        if (false !== $process) {
            $numCpus = intval(fgets($process));

            pclose($process);
        }

        return intval($numCpus);
    }

    /**
     * Get number of cpus for mac base system.
     *
     * @return mixed
     */
    private function getCpuNumberFromMac()
    {
        $numCpus = 1;

        $process = @popen('sysctl -a', 'rb');

        if (false !== $process) {
            $output = stream_get_contents($process);

            preg_match('/hw.ncpu: (\d+)/', $output, $matches);
            if ($matches) {
                $numCpus = intval($matches[1][0]);
            }

            pclose($process);
        }

        return intval($numCpus);
    }
}
