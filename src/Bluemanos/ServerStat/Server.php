<?php

namespace Bluemanos\ServerStat;

use Bluemanos\ServerStat\Components\Cpu;
use Bluemanos\ServerStat\Components\Hdd;
use Bluemanos\ServerStat\Components\PresentationTrait;
use Bluemanos\ServerStat\Components\Ram;

/**
 * Get bunch of server statistics - hdd usage, cpu usage, ram usage, etc.
 */
class Server
{
    use PresentationTrait;

    /**
     * number precision.
     *
     * @var int
     */
    private $roundPrecision = 0;

    private $cpu;
    private $hdd;
    private $ram;

    /**
     * Server constructor.
     */
    public function __construct()
    {
        $this->cpu = new Cpu();
        $this->hdd = new Hdd();
        $this->ram = new Ram();
    }

    /**
     * Get disk free space.
     *
     * @param string $path
     * @param bool $humanReadable
     * @return string
     */
    public function freeSpace($path = '/', $humanReadable = true)
    {
        return $this->hdd->freeSpace($path, $humanReadable);
    }

    /**
     * Get disk total space.
     *
     * @param string $path
     * @param bool $humanReadable
     * @return string
     */
    public function totalSpace($path = '/', $humanReadable = true)
    {
        return $this->hdd->totalSpace($path, $humanReadable);
    }

    /**
     * Get hdd space in percent - usage vs. total.
     *
     * @return mixed
     */
    public function hddPercent()
    {
        return round($this->hdd->hddPercent(), $this->roundPrecision);
    }

    /**
     * Get server load.
     *
     * @param bool $compact
     * @return mixed
     */
    public function load($compact = true)
    {
        return $this->cpu->load($compact);
    }

    /**
     * Get server load in percent.
     *
     * @return mixed
     */
    public function loadPercent()
    {
        return round($this->cpu->loadPercent(), $this->roundPrecision);
    }

    /**
     * Returns the number of available CPU cores
     * Should work for Linux, Windows, Mac & BSD.
     *
     * @return int
     */
    public function cpusNumber()
    {
        return $this->cpu->cpusNumber();
    }

    /**
     * Get php memory usage.
     *
     * @param bool $humanReadable
     * @return string
     */
    public function getMemoryPhpUsage($humanReadable = true)
    {
        return $this->ram->getMemoryPhpUsage($humanReadable);
    }

    /**
     * Get server memory data.
     *
     * @param bool $humanReadable
     * @return array
     */
    public function getMemoryServer($humanReadable = true)
    {
        return $this->ram->getMemoryServer($humanReadable);
    }

    /**
     * Get server memory in percent.
     *
     * @return mixed
     */
    public function memoryServerPercent()
    {
        return round($this->ram->memoryServerPercent(), $this->roundPrecision);
    }
}
