<?php

class LibraryServerTest extends PHPUnit_Framework_TestCase
{
    /** @var \Bluemanos\ServerStat\Server */
    protected $server;

    public function setUp()
    {
        $this->server = new \Bluemanos\ServerStat\Server();
    }

    public function testFreeSpace()
    {
        $this->assertEquals(disk_free_space('/'), $this->server->freeSpace('/', false));
    }

    public function testTotalSpace()
    {
        $this->assertEquals(disk_total_space('/'), $this->server->totalSpace('/', false));
    }

    public function testHddPercent()
    {
        $freeSpace = disk_free_space('/');
        $totalSpace = disk_total_space('/');
        $expectedValue = round((($totalSpace - $freeSpace) / $totalSpace) * 100);

        $this->assertEquals($expectedValue, $this->server->hddPercent());
    }

    public function testLoad()
    {
        $this->assertEquals(sys_getloadavg(), $this->server->load(false));

        $this->assertInternalType('string', $this->server->load());

        $expectedValue = implode(' ', array_map(function ($var) { return round($var, 2); }, $this->server->load(false)));
        $this->assertEquals($expectedValue, $this->server->load());
    }

    /**
     * @requires OS Linux
     */
    public function testCpuNumberLinux()
    {
        $cpuinfo = file_get_contents('/proc/cpuinfo');
        preg_match_all('/^processor/m', $cpuinfo, $matches);
        $numCpus = count($matches[0]);

        $this->assertEquals(intval($numCpus), $this->server->cpusNumber());
    }

    public function testLoadPercent()
    {
        $expectedValue = ($this->server->load(false)[0] / $this->server->cpusNumber()) * 100;
        $this->assertEquals(round($expectedValue), $this->server->loadPercent());
    }

    public function testMemoryPhpUsage()
    {
        //$this->assertEquals(memory_get_usage(), $this->server->getMemoryPhpUsage(false));
        $this->assertInternalType('int', $this->server->getMemoryPhpUsage(false));
    }
}
