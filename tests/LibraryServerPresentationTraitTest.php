<?php

class LibraryServerPresentationTraitTest extends PHPUnit_Framework_TestCase
{
    /** @var Bluemanos\ServerStat\Components\PresentationTrait */
    protected $mock;

    public function setUp()
    {
        $this->mock = $this->getMockForTrait('Bluemanos\ServerStat\Components\PresentationTrait');
    }

    public function testColorProgressSuccess()
    {
        $this->assertEquals('progress-bar-success', $this->mock->colorProgress(1));
        $this->assertEquals('progress-bar-success', $this->mock->colorProgress(20));
        $this->assertEquals('progress-bar-success', $this->mock->colorProgress(50));
    }

    public function testColorProgressWarning()
    {
        $this->assertEquals('progress-bar-warning', $this->mock->colorProgress(51));
        $this->assertEquals('progress-bar-warning', $this->mock->colorProgress(60));
        $this->assertEquals('progress-bar-warning', $this->mock->colorProgress(84));
    }

    public function testColorProgressDanger()
    {
        $this->assertEquals('progress-bar-danger', $this->mock->colorProgress(85));
        $this->assertEquals('progress-bar-danger', $this->mock->colorProgress(90));
        $this->assertEquals('progress-bar-danger', $this->mock->colorProgress(120));
    }

    public function testHumanFileSize()
    {
        $this->assertEquals('1.0 B', $this->mock->humanFileSize(1));
        $this->assertEquals('1 B', $this->mock->humanFileSize(1, 0));

        $this->assertEquals('112.5 MB', $this->mock->humanFileSize(117948174));
        $this->assertEquals('112 MB', $this->mock->humanFileSize(117948174, 0));

        $this->assertEquals('2.5 YB', $this->mock->humanFileSize(2984579243857349275293452));
        $this->assertEquals('2 YB', $this->mock->humanFileSize(2984579243857349275293452, 0));
        $this->assertEquals('2.46879 YB', $this->mock->humanFileSize(2984579243857349275293452, 5));
    }
}
